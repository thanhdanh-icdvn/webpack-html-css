module.exports = {
  'files':[
    '../../assets/icons/*.svg',
  ],
  'fontName':'IconFont',
  'classPrefix':'icon-',
  'baseSelector':'.icon',
  'types': ['eot', 'woff', 'woff2', 'ttf', 'svg'],
  'fileName': 'assets/fonts/icons/[fontname].[ext]',
  'fontHeight': 1024
}